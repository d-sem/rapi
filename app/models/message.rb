class Message < ApplicationRecord
  validates :text, presence: true
  validates :user_id, presence: true
  belongs_to :user, optional: true
end
