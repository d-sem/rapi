class UsersController < ApplicationController
  before_action :authenticate_request, except: [:create]
  before_action :set_user, only: [:show, :update, :destroy]

  # GET /users
  def index
    if params[:user_id]
      id = AuthorizeApiRequest.call(request.headers).result[:id]
      render json: id
    else
      columns = User.column_names - ['password_digest']
      @users = User.select(columns)
      render json: @users
    end
  end

  # GET /users/1
  def show
    render json: @user
  end

  # POST /user
  def create
    @user = User.new(user_params)

    if @user.save
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  private

  def set_user
    @user = User.find(params[:id]).attributes.except('password_digest')
  end

  def user_params
    params.permit(:name, :email, :password, :password_confirmation)
  end
end