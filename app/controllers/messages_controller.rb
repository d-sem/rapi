class MessagesController < ApplicationController
  before_action :set_message, only: [:show, :update, :destroy]

  # GET /messages?from=:id
  def index
    if params[:last]
      @messages = Message.last(params[:last])
    elsif params[:from] && params[:from]
      @messages = Message.where(id: params[:from]..params[:to])
    else
      @messages = Message.all
    end
    render json: @messages
  end

  # GET /messages/1
  def show
    render json: @message
  end

  # POST /messages
  def create
    @message = Message.new(message_params)
    @current_user = AuthorizeApiRequest.call(request.headers).result
    puts @current_user[:id].inspect
    @message[:user_id] = @current_user[:id]

    puts @message.inspect
    if @message.save
      render json: @message, status: :created, location: @message
    else
      render json: @message.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /messages/1
  def update
    if user_is_owner?
      if @message.update(message_params)
        render json: @message, status: :no_content
      else
        render json: @message.errors, status: :unprocessable_entity
      end
    else
      render json: @message.errors, status: :unprocessable_entity
    end
  end

  # DELETE /messages/1
  def destroy
    if user_is_owner?
      @message.destroy
      render status: :no_content
    else
      render json: @message.errors, status: :unprocessable_entity
    end
  end

  private

  def set_message
    @message = Message.find(params[:id])
  end

  def message_params
    params.permit(:text)
  end

  def user_is_owner?
    id = AuthorizeApiRequest.call(request.headers).result[:id]
    if @message[:user_id] != id
      puts "wrong user"
      @message.errors.add(:user, message: 'wrong user')
      false
    else
      puts "correct user"
      true
    end
  end
end
